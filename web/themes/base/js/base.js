/**
 * @file
 * Placeholder file for custom sub-theme behaviors.
 *
 */
(function ($, Drupal) {

  /**
   * Use this behavior as a template for custom Javascript.
   */
  Drupal.behaviors.exampleBehavior = {
    attach: function (context, settings) {
      //alert("I'm alive!");
    }
  };

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.cookies = {
        attach: function (context, settings) {
            $(document).ready(function() {
                $('.decline-button').on('click', function(){
                    $('#sliding-popup').remove();
                });
            });
        }
    };

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.popUp = {
        attach: function (context, settings) {
            $(context).find('#block-popup').once('ifPopUp').each(function () {
                var cookiePopUpCheck = $.cookie("popUp-check");

                function checkCookie() {
                    cookiePopUpCheck = $.cookie("popUp-check", 1, {expires: 2});
                }
                if (!cookiePopUpCheck) {
                    $('#block-popup').foundation('open');
                }

                $('#close-pop-up').on('click', function () {
                    $("#block-popup").on("closed.zf.reveal", function (e) {
                        checkCookie();
                    });
                    $('#block-popup').foundation('close');
                });
            });
        }
    }

    /**
     * Use this behavior as a template for custom Javascript.
     */
    Drupal.behaviors.slides = {
        attach: function (context, settings) {
            $(context).find('.field-name-field-slides.swiper-container').once('ifLatestSlider').each(function (){
                var slides = new Swiper ('.field-name-field-slides.swiper-container', {
                    autoplay: true,
                    loop: true,
                    effect: 'fade',
                    pagination: {
                        el: '.swiper-pagination',
                        type: 'bullets',
                        clickable: true,
                    },
                });
            });
        }
    };

    Drupal.behaviors.slidesbasic = {
        attach: function (context, settings) {
                var slides = new Swiper('.field-name-field-image.swiper-container', {
                    autoplay: true,
                    loop: true,
                    effect: 'fade',
                    pagination: {
                        el: '.swiper-pagination',
                        type: 'bullets',
                        clickable: true,
                    },
                          navigation: {
                            nextEl: '.swiper-button-next',
                            prevEl: '.swiper-button-prev',
                          },
                });
        }
    };

    Drupal.behaviors.imagesLoaded = {
        attach: function (context, settings) {
            $('.grid').imagesLoaded(function () {
                $('.grid').masonry({
                    itemSelector: '.grid-item',
                    fitWidth: true,
                    gutter: 20
                });
            });
        }
    };

    Drupal.behaviors.referencesLightgallery = {
        attach: function (context, settings) {
            $(document).ready(function () {
                $(".lightboxgallery").lightGallery();
            });
        }
    };
    /**
     * offCanvas behaviors
     */
    Drupal.behaviors.offCanvas = {
        attach: function (context, settings) {
            $(context).find('#offCanvasRightOverlap').once('ifRightCanvas').each(function () {
                $menuButton = $('#menuButton');
                $(this).on("opened.zf.offcanvas", function(e){
                    $menuButton.addClass('is-active');
                });
                $(this).on("closed.zf.offcanvas", function(e){
                    $menuButton.removeClass('is-active');
                });
            });
        }
    };

})(jQuery, Drupal);
